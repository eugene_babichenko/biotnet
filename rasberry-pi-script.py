#! /usr/bin/env python3

import serial
from sshtunnel import SSHTunnelForwarder
import paho.mqtt.client as mqtt

MQTT_SERVER = "185.51.246.78"
MQTT_SSH_PORT = 22
MQTT_LOGIN = "root"
MQTT_PKEY = "~/.ssh/biotnet_emc_raspberry_pi"
MQTT_SERVER_PORT = 1884
MQTT_LOCAL_PORT = 22886
PORT = "/dev/ttyACM1"
BAUDRATE = 9600

TOPIC_SWITCH = "smart-house/out/lamp_switch"
TOPIC_LEVEL = "smart-house/out/lamp_level"

# Serial port
serial_port = serial.Serial(PORT, BAUDRATE, timeout=1)
# A fix to work on older versions of pyserial
try:
    if not serial_port.is_open:
        serial_port.open()
except AttributeError:
    if not serial_port.isOpen():
        serial_port.open()

# SSH Tunneling
tunnel = SSHTunnelForwarder(
    (MQTT_SERVER, MQTT_SSH_PORT),
    ssh_username=MQTT_LOGIN,
    ssh_pkey=MQTT_PKEY,
    remote_bind_address=("127.0.0.1", MQTT_SERVER_PORT),
    local_bind_address=("127.0.0.1", MQTT_LOCAL_PORT)
)
tunnel.start()


# MQTT Connection
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe(TOPIC_SWITCH)
    client.subscribe(TOPIC_LEVEL)


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))

    if msg.topic == "smart-house/out/lamp_switch":
        serial_port.write(b"e\n" + msg.payload + b"\n")
    elif msg.topic == "smart-house/out/lamp_level":
        serial_port.write(b"l\n" + msg.payload + b"\n")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1", MQTT_LOCAL_PORT, 60)

client.loop_forever()
tunnel.stop()
serial_port.close()
