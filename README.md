# A project to demonstrate emcSSH usage for creating a secure MQTT channel

## Motivation

The goal of the project was to get rid of certification authority. The
implementation is based on the idea of using Emercion blockchain as CA. This is
acceptable because any data written to a blockchain can never be changed by
third-parties.

## Why Emercoin

Emercoin provides the solution for building data storages on top of blockchain
called NVS (name-value storage). The Emercoin stack also contains emcssh -
the software, that uses NVS for storing public keys and is tightly integrated
with sshd. So the only things we need to do are to run emcssh on the server and
to build a SSH tunnel between our client and server.

## Scripts included
* `raspberry-pi-script.py` - connects a server and hardware via a SSH tunnel
* `client.py` - a simple UI

The testing hardware: Raspberry Pi with SSH client running on it. RPi sends
commands to low-level hardware which controls two LEDs (as a demo of distant
lights control).

## Links

* https://wiki.emercoin.com/en/EMCSSH#Setting_up_EMCSSH
* https://pypi.python.org/pypi/sshtunnel
