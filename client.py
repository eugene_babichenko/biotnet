#! /usr/bin/env python3

from sshtunnel import SSHTunnelForwarder
import paho.mqtt.client as mqtt
import tkinter

MQTT_SERVER = "185.51.246.78"
MQTT_SSH_PORT = 22
MQTT_LOGIN = "root"
MQTT_PKEY = "~/.ssh/biotnet_emc_working_pc"
MQTT_SERVER_PORT = 1884
MQTT_LOCAL_PORT = 22883

# SSH Tunneling
tunnel = SSHTunnelForwarder(
    (MQTT_SERVER, MQTT_SSH_PORT),
    ssh_username=MQTT_LOGIN,
    ssh_pkey=MQTT_PKEY,
    remote_bind_address=("127.0.0.1", MQTT_SERVER_PORT),
    local_bind_address=("127.0.0.1", MQTT_LOCAL_PORT)
)
tunnel.start()


# MQTT Connection
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("$SYS/#")


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1", MQTT_LOCAL_PORT, 60)
client.loop_start()


# Interface callback functions
def lamp_switch(mqtt_client, lamp_n):
    mqtt_client.publish("smart-house/out/lamp_switch", payload=str(lamp_n))


def lamp_set_level(mqtt_client, lamp_n, level):
    mqtt_client.publish("smart-house/out/lamp_level", payload=str(lamp_n) + " " + str(level))


# User interface implementation goes here
root = tkinter.Tk()

slider_lamp1 = tkinter.Scale(from_=0, to=100, orient=tkinter.HORIZONTAL)
slider_lamp1.bind("<ButtonRelease-1>", lambda event: lamp_set_level(client, 1, slider_lamp1.get()))
slider_lamp1.pack()
button_lamp1 = tkinter.Button(text="Switch 1")
button_lamp1.pack()
button_lamp1.bind("<Button-1>", lambda event: lamp_switch(client, 1))

slider_lamp2 = tkinter.Scale(from_=0, to=100, orient=tkinter.HORIZONTAL)
slider_lamp2.bind("<ButtonRelease-1>", lambda event: lamp_set_level(client, 2, slider_lamp2.get()))
slider_lamp2.pack()
button_lamp2 = tkinter.Button(text="Switch 2")
button_lamp2.pack()
button_lamp2.bind("<Button-1>", lambda event: lamp_switch(client, 2))

root.mainloop()
client.loop_stop()
tunnel.stop()
